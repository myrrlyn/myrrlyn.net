---
title: Résumé
category: local
---

# Résumé

## Formal Education

**Trine University:** Angola, IN; August 2011 to May 2016. Computer Engineering
major.

Notable courses:

- ***Senior Design***: My capstone project was the construction and programming
of an autonomous cargo hauler. I worked with three mechanical engineers on the
physical machine, and personally built the electrical and software systems. The
machine capably navigated along a pre-programmed route, and successfully
detected obstacles such as pedestrians and a train. The code is on my GitHub
profile, and the summary video is [on YouTube][1]. I can provide design
documents upon request.

- **Logic and Computer Design**: This class focused on the hardware construction
of a computer and the instruction primitives executing on it. We built a
five-stage MIPS CPU in Verilog.

- **Embedded Systems**: This class taught real-time operating systems and
OS-less programming. We built an oscilloscope and basic RTOS.

- **Software Engineering I and II**: These classes focused on development
patterns, software design, and algorithm use. We learned C++/Qt and Ruby, and
built a Rails app and PostgreSQL database.

General courses:

- *Mathematics*: Calculus III, Linear Algebra, Differential Equations, Graph
Theory and Combinatorics, Statistics, Random Processes

- *Programming*: Object-Oriented Programming (Java), Application Programming
(C++/Qt), Computer Security (Java), Net-Centric Computing (PHP, C++), Algorithms
(C++, Python).

- *Electronic Systems*: Circuit Theory and Analysis, Digital Systems (Verilog),
Microcontrollers (C, Assembly).

Mechanical Engineering major (two years):

- Static Analysis, Solid Mechanics, Dynamic Systems
- Thermodynamics
- Kinematics and Mechanical Linkages
- Mechatronics

## Skills

I rapidly master development environments, toolchains, and programming
languages. I also readily understand software abstract principles such as
algorithm design, flow of control, design patterns, and behavioral paradigms.

I taught myself the basics of programming and Linux use before switching majors,
and continued to complement my formal education with individual pursuits and
projects. I have continued pursuing education on my own initiative, and am
capable of working with a broad range of languages, tools, and systems.

List items with a “…” following them contain additional information. Click on an
item to show or hide its content, or click here to expand all items.

<button id="cover-toggle">Toggle all “…” items.</button>

### Languages

- English: I have an excellent command of English and technical communication.

- Ruby
    {:.cover}

    I taught myself Ruby before taking the computer engineering major, and
    continue to use it as my prototyping language. I am familiar with Ruby on
    Rails, and have written gems for public use. I am currently using Ruby at
    work to drive the ground end of one of our satellite projects.

- Java
    {:.cover}

    I learned the basics of Java in high school, and pursued it further for
    my formal courses in it.

- C, C++
    {:.cover}

    I am most familiar with the language cores, as I did most of my embedded
    work with little or no standard library presence. I am also familiar with
    the Qt framework, and have built both integrated and client/server
    applications using it.

    - AVR Assembly
        {:.cover}

        My embedded classes taught us to use ASM to start our controllers,
        implement base drivers, and hand-optimize certain routines.

- Rust
    {:.cover}

    I investigated using Rust for my Senior Design project. I taught myself Rust
    and am fluent in its core concepts. I have used it for approximately two
    years now, and have a [published crate][2] demonstrating my knowledge of
    various language features. I have also deployed a Rust project for data
    stream processing at work, though I am not at liberty to disclose its code.

- C♯:
    {:.cover}

    I taught myself C♯ and Visual Studio, and have used them to build console
    apps and ASP.NET websites. I have also used the graphical frameworks to
    build applications.

- HTML/CSS/JavaScript
    {:.cover}

    I taught myself web design, and volunteered to help my professor teach these
    languages for my Software Engineering classes. I write this site myself, and
    offer it as a testament to my ability.

#### Others

- Bash and Zsh scripting
- Python
- AVR, MIPS, x86 assemblers

### Systems and Tools

- Windows
    {:.cover}

    I’ve used Windows all my life, and have some experience in system
    administration and Windows Server usage. I currently only use Windows at
    work.

- GNU/Linux
    {:.cover}

    I started on Ubuntu, and now run Arch Linux as my daily-use system. I am
    comfortable working with GNU userspace tools on the command line. I have
    written several shell scripts and systemd units for system administration. I
    use Linux both as my desktop system and on my server.

- OSX / macOS
    {:.cover}

    I am comfortable using Macintosh’s graphical and terminal environments.

- Microsoft Office
    {:.cover}

    I have used Word, Excel, and Outlook since elementary school, and am
    proficient in their use. I am skilled with Powerpoint and familiar with
    Access.

- Git
    {:.cover}

    I learned Git the hardest way: on a team project, with my partner in Texas,
    by experimentation. I have several years of experience using Git and have a
    firm understanding of its theory and actual usage. I use Git for all my
    work.

- Command shells
    {:.cover}

    I use Zsh as my daily shell, and am familiar with GNU Bash as well. I use
    PowerShell at work.

- Editors/IDEs
    - Atom and Microsoft Visual Studio Code (not at all related to VS)
    - Basic Vim usage
    - GNU Nano
    - Eclipse and NetBeans
    - Microsoft Visual Studio

## Work Experience

### Rota-Kiwan Scout Reservation

I worked at Rota-Kiwan Scout Reservation over the summers during college, as a
lifeguard and then aquatics director. My responsibilities included:

- Recruiting, interviewing, and hiring staff
- Managing area resources ($30,000 of material and capital assets)
- Maintaining area safety
- Coordinating emergency response with campers, staffers, and emergency services
- Training my staff members
- Enforcing safety protocols for my area and

I received excellent reviews from the scouts, parents, and my management. My
technical capabilities are largely self-evident, from projects I’ve already done
and through any technical interviews. My abilities as an employee, team member,
and leader are less quantifiable. I can provide references to these effects upon
request.

### Space Dynamics Laboratory

I wrote middleware networking drivers for the NASA BioSentinel mission. This
required working knowledge of POSIX I/O driver structure, VxWorks kernel use,
and SPARCv8 performance. My code was written in ISO C99.

I am currently working with Ruby to orchestrate ground operations for another
mission, about which I know little and can disclose less.

## Professional Certifications and Associations

- Professional Association of Diving Instructors
    - Rescue Diver
    - Advanced Open Water Diver
    - *Enriched Air/Nitrox Diver*
    - *Wreck Diver*
- American Red Cross
    - ARC Lifeguard
    - CPR and Emergency First Aid Provider
- Boy Scouts of America
    - Aquatics Instructor
    - Lifeguard Instructor
    - BSA Lifeguard

[1]: https://www.youtube.com/watch?v=K3CKSovJbJQ
[2]: https://crates.io/crates/endian_trait
