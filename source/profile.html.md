---
title: Profile
category: local
---

# About Me

I was born and raised in rural Minnesota, about an hour southwest of the Twin
Cities. I’ve always displayed a natural thirst for knowledge, especially about
music, mathematics, and computing, and my parents encouraged the pursuit of
education both in and out of the classroom. Living in the country provided
opportunities to learn about carpentry (my dad was a hobbyist cabinetmaker, and
we also had to do a ***lot*** of upkeep on the property) and the outdoors with
the Boy Scouts.

My family moved to Michigan in 2005. I completed my Eagle Scout Service Project
in 2008, which reintroduced bluebirds to my local county for the first time in
over twenty years. I attended Trine University, originally as a Mechanical
Engineer. When I realized I was much more passionate about the programming and
machine control than by the finished products, I switched to Computer
Engineering. My Senior Design project, the construction and programming of a
large autonomous vehicle, involved my skills in both disciplines.

In addition to my academic and technical pursuits, I also greatly enjoy music
and aquatics. I began learning piano when I was six, and added the trumpet and
French horn in middle and high school, respectively. I’ve played in my school
bands for ten years and would like to continue in a community orchestra wherever
I wind up. I have been a strong swimmer all my life, and became a Red Cross
lifeguard at 14. My passion for aquatic safety has led me to become a BSA
lifeguard and PADI Rescue Diver, and I have employed these skills as the
aquatics director at a local Boy Scout camp. During my time at Rota-Kiwan, I
received excellent reviews and maintained a strong safety record.

I am deeply passionate about achieving safety in risky environments through
familiarity, competence, and assistance. I aim to make delivery of such safety
benefits a primary goal of my career and personal life.

# Personal Life

I haven’t yet figured out how to make a vibrant personal life for myself as a
transplant in Utah, as I am not of the LDS church nor am I a student at Utah
State University, so that’s something on which I’ll need to work in the next few
years. I am an assistant scoutmaster with Troop 1, which I tremendously enjoy,
and I have a few friends my own age from work. Mostly I’m just glad that several
of my friends from back in Michigan still speak to me.

I am very excited about all the new geography to explore around me, as well as
the multitude of national parks near by. So far I’ve only been to Arches
National Park, but I am far too Boy Scout to hold out on all the rest for long.

# Career Goals

I’m almost a year into my first job at [Space Dynamics][1], and I’m very happy
with both the work I’m doing and the company culture there. I currently don’t
have any plans to switch jobs or move out of Logan, but, who knows. I’m only 24
and as yet unwed.

I am interested in the [Rust programming language][2] for its memory safety and
performance characteristics, both of which are rather important in aerospace
work. Rust isn’t *quite* ready to displace C/C++ in flight software, as it uses
LLVM, which has fewer targets than GCC, and it is not yet readily integrable with
build systems like CMake.

I’d like to start getting Rust’s foot in the door in my workplace, including if
necessary (and possible for me to do…) contributing to Rust in areas that we
identify it as needing work.

[1]: http://sdl.usu.edu/
[2]: https://www.rust-lang.org/
